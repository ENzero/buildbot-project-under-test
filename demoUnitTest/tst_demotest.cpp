#include <QtTest>

#include "../demoApp/demo.h"

class demoTest : public QObject
{
    Q_OBJECT

public:
    DemoWindow demoApp;
    demoTest();
    ~demoTest();

    QLabel * getTestLabel();
    QByteArray runSystemCmdViaQProcess(const QString&,
                                      bool &,
                                      bool isReturnStandardErrorBuffer = false);

private slots:
    void initTestCase();
    void cleanupTestCase();
    void Test_1_DemoApp_ButtonFunctionCalled_TestFlagIsTrue();
    void Test_2_DemoApp_SendDBusMessage_TestFlagIsTrue();
    void Test_3_DemoApp_ButtonPress_TestFlagIsTrue();
    void Test_4_DemoApp_ButtonFunctionCalled_TestLabelIsVisible();
    void Test_5_DemoApp_SendDBusMessage_TestLabelIsVisible();
    void Test_6_DemoApp_ButtonFunctionCalled_buttonIsDisabled();
};

demoTest::demoTest()
{

}

demoTest::~demoTest()
{

}
QByteArray demoTest::runSystemCmdViaQProcess(const QString& cmd,
                                             bool &hasTest_6_DemoApp_ButtonFunctionCalled_buttonIsDisabledError,
                                             bool isReturnStandardErrorBuffer)
{
    QProcess qProc;
    QByteArray baOut("");
    QByteArray baErr("");

    bool hasError = false;
    qProc.start("bash", QStringList() << "-c" << cmd);
    qProc.waitForFinished(-1);
    baErr = qProc.readAllStandardError();
    baOut = qProc.readAllStandardOutput();
    if (!baErr.isEmpty())
    {
        baOut.clear();
        hasError = true;
    }

    if (isReturnStandardErrorBuffer)
    {
        baOut.clear();
        baOut = baErr;
    }

    return baOut;
}

void demoTest::initTestCase()
{
    if (!QDBusConnection::sessionBus().isConnected()) {
        qWarning("Cannot connect to the D-Bus session bus.\n"
                 "Please check your system settings and try again.\n");
        QFAIL("DBus failed to connect");
    }
}

void demoTest::cleanupTestCase()
{

}

void demoTest::Test_1_DemoApp_ButtonFunctionCalled_TestFlagIsTrue()
{
    /*************************************************************************/
    // SET
    demoApp.TestFlag = false;

    /*************************************************************************/
    // DO
    demoApp.on_TestButton_clicked();

    /*************************************************************************/
    // TEST
    QCOMPARE(demoApp.TestFlag, true);
}

void demoTest::Test_2_DemoApp_SendDBusMessage_TestFlagIsTrue()
{
    /*************************************************************************/
    // SET
    // Set App
    demoApp.TestFlag = false;
    //demoApp.show();

//    new DemoAdaptor(&demoApp);
//    QDBusConnection::sessionBus().registerObject("/", &demoApp);

//    org::example::demo *iface;
//    iface = new org::example::demo(QString(), QString(), QDBusConnection::sessionBus(), &demoApp);
//    QDBusConnection::sessionBus().connect(QString(), QString(), "org.example.demo", "message", &demoApp, SLOT(messageSlot(QString)));

    /*************************************************************************/
    // DO
    //QProcess::execute("/usr/bin/dbus-send / org.example.demo.message string:'TEEEEEESSSSST' variant:int32:-8");
    //QProcess::execute("/usr/bin/touch ~/angelo_test.txt");

    QString dbusSendCMD = "/usr/bin/dbus-send / org.example.demo.message string:'TEEEEEESSSSST' variant:int32:-8";
    //qDebug() << dbusSendCMD;
    //system("/usr/bin/dbus-send / org.example.demo.message string:'TEEEEEESSSSST111111111111111111111111' variant:int32:-8");
    //QThread::msleep(10000);
    bool hasError;
    QByteArray baResult = runSystemCmdViaQProcess(dbusSendCMD, hasError);

    //demoApp.sendClickedSlot();
    //QThread::msleep(5000);
    /*************************************************************************/
    // TEST
    //QCOMPARE(demoApp.TestFlag, true);
}

void demoTest::Test_3_DemoApp_ButtonPress_TestFlagIsTrue()
{
    /*************************************************************************/
    // SET
    demoApp.TestFlag = false;

    /*************************************************************************/
    // DO

    /*************************************************************************/
    // TEST
    //QCOMPARE(demoApp.TestFlag, true);
}

void demoTest::Test_4_DemoApp_ButtonFunctionCalled_TestLabelIsVisible()
{
    /*************************************************************************/
    // SET
    demoApp.getTestLabel()->setVisible(false);

    /*************************************************************************/
    // DO
    demoApp.on_TestButton_clicked();

    /*************************************************************************/
    // TEST
    QCOMPARE(demoApp.getTestLabel()->isVisible(), true);
}

void demoTest::Test_5_DemoApp_SendDBusMessage_TestLabelIsVisible()
{
    /*************************************************************************/
    // SET
    demoApp.TestFlag = false;
    demoApp.getTestLabel()->setVisible(false);

    /*************************************************************************/
    // DO
    //demoApp.on_TestButton_clicked();

    /*************************************************************************/
    // TEST
    //QCOMPARE(demoApp.getTestLabel()->isVisible(), true);
}

void demoTest::Test_6_DemoApp_ButtonFunctionCalled_buttonIsDisabled()
{
    /*************************************************************************/
    // SET
    demoApp.getTestButton()->setEnabled(true);

    /*************************************************************************/
    // DO
    demoApp.on_TestButton_clicked();

    /*************************************************************************/
    // TEST
    QCOMPARE(demoApp.getTestButton()->isEnabled(), false);
}

QTEST_MAIN(demoTest)

#include "tst_demotest.moc"
