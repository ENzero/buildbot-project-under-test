QT += dbus widgets testlib
#QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
# CONFIG -= app_bundle

TEMPLATE = app

HEADERS += ../demoApp/demo.h
SOURCES += ../demoApp/demo.cpp
FORMS += ../demoApp/demowindow.ui

TEST_TARGET = ../App/demoApp
DBUS_ADAPTORS += ../demoApp/org.example.demo.xml
DBUS_INTERFACES += ../demoApp/org.example.demo.xml

# Test Sources
SOURCES +=  tst_demotest.cpp

#target.path = ../demoUnitTest
INSTALLS += target
