QMAKE=/home/client/Qt/5.9.9/gcc_64/bin/qmake
DEMO_APP_PROJECT=./demoApp/demo.pro
UNIT_TEST_PROJECT=./demoUnitTest/demoTester.pro

rm \
./demo.o \
./demo_adaptor.cpp \
./demo_adaptor.h \
./demo_adaptor.o \
./demo_interface.cpp \
./demo_interface.h \
./demo_interface.o \
./main.o \
./Makefile \
./moc_demo.cpp \
./moc_demo.o \
./moc_demo_adaptor.cpp \
./moc_demo_adaptor.o \
./moc_demo_interface.cpp \
./moc_demo_interface.o \
./moc_predefs.h \
./target_wrapper.sh \
./tst_demotest.moc \
./ui_demowindow.h \
./.qmake.stash \
./tst_demotest.o \
./demo \
./demoTester


echo "Building DemoApp"
$QMAKE $DEMO_APP_PROJECT -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make


echo "Buidling DemoTester"
$QMAKE $UNIT_TEST_PROJECT -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make



