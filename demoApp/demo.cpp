#include "demo.h"

DemoWindow::DemoWindow()
{
    setupUi(this);
    sendButton->setEnabled(false);
    sendButton->setVisible(false);
    helloWorldLabel->setVisible(false);
    TestButton->setEnabled(true);
    this->showFullScreen();

    connect(sendButton, SIGNAL(clicked(bool)), this, SLOT(sendClickedSlot()));

    // Add our D-Bus interface and connect to D-Bus
    new DemoAdaptor(this);
    QDBusConnection::sessionBus().registerObject("/", this);

    org::example::demo *iface;
    iface = new org::example::demo(QString(), QString(), QDBusConnection::sessionBus(), this);
    QDBusConnection::sessionBus().connect(QString(), QString(), "org.example.demo", "message", this, SLOT(messageSlot(QString)));
    qDebug() << "Init done!!!";
}

DemoWindow::~DemoWindow()
{
}

void DemoWindow::messageSlot(const QString &text)
{
    qDebug() << "ANGELO:RECEIVED!!!!!";
    on_TestButton_clicked();
}

void DemoWindow::sendClickedSlot()
{
    qDebug() << "SEEEENNDDD!!!";
    QDBusMessage msg = QDBusMessage::createSignal("/", "org.example.demo", "message");
    msg << "ANGELO";
    QDBusConnection::sessionBus().send(msg);
}

void DemoWindow::on_TestButton_clicked()
{
    qDebug("Pressed test button.");
    TestFlag = true;
    helloWorldLabel->setVisible(true);
    TestButton->setEnabled(false);
}

QLabel * DemoWindow::getTestLabel()
{
qDebug() << "ANGELO:RECEIVED!!!!!dddd";
    return helloWorldLabel;
}

QPushButton * DemoWindow::getTestButton()
{
    return TestButton;
}
