QT += dbus widgets

HEADERS += demo.h
SOURCES += demo.cpp \
    main.cpp
FORMS += demowindow.ui

DBUS_ADAPTORS += org.example.demo.xml
DBUS_INTERFACES += org.example.demo.xml
